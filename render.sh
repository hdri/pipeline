#!/bin/bash

# $ cd temp
# $ ../render.sh 000001_out.exr 10 1 render10.exr
#   will produce render10.exr in current folder,
#    - using 000001_out.exr (from current folder) as an environment map
#       rotated 10 deg around Z axis
#    - using 1 sample per pixel

DIR="$(dirname "$(realpath "$0")")"

. "$DIR/_settings.sh"

if [ $# -lt 4 ]; then
	echo "Usage: $0 <environment_map> <rotation_z> <sample_count> <output_filename>"
	exit 1
fi

ENVIRONMENT_MAP=$(realpath "$1")
ROTATION_Z="$2"
SAMPLE_COUNT="$3"
OUTPUT_FILENAME="$4"

SCENE=$(cat <<END

# modified PbrtMetalTeapot test scene

LookAt 15 4.5 2    15 .9 10   0 1 0
Camera "perspective" "float fov" [25]

PixelFilter "box"

Film "image" "integer xresolution" [$RENDER_WIDTH] "integer yresolution" [$RENDER_HEIGHT]
    "string filename" "$OUTPUT_FILENAME"

Sampler "lowdiscrepancy" "integer pixelsamples" $SAMPLE_COUNT

WorldBegin

# lights
AttributeBegin
Rotate -90 1 0 0
Rotate $ROTATION_Z 0 0 1
    LightSource "infinite" "integer nsamples" [16] "color L" [1 1 1]
        "string mapname" ["$ENVIRONMENT_MAP"]
AttributeEnd

# floor
Texture "checks" "spectrum" "checkerboard"
          "float uscale" [55] "float vscale" [55]
          "rgb tex1" [.1 .1 .1] "rgb tex2" [.8 .8 .8]
Material "matte" "texture Kd" "checks"
Shape "trianglemesh" "integer indices" [0 1 2 0 3 2 ]
    "point P" [ -100 0 -100  100 0 -100   100 0 100   -100 0 100 ]

# left sphere
AttributeBegin
Material "metal"  "float roughness" [.01]
Translate 11 1 10
Scale 1.25 1.25 1.25
Shape "sphere" "float radius" [1]
AttributeEnd

# middle sphere
AttributeBegin
Material "metal"  "float roughness" [.001]
Translate 15 1 10
Scale 1.25 1.25 1.25
Shape "sphere" "float radius" [1]
AttributeEnd

# right sphere
AttributeBegin
Material "mirror"
Translate 19 1 10
Scale 1.25 1.25 1.25
Shape "sphere" "float radius" [1]
AttributeEnd


WorldEnd

END
)

#echo "$SCENE"

# Pass the scene to the renderer
echo "$SCENE" | "$PBRT_BIN"