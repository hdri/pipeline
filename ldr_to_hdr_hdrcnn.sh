#!/bin/bash

#DIR=$(dirname "$0")
DIR="$(dirname "$(realpath "$0")")"

. "$DIR/_settings.sh"

if [ $# -lt 2 ]; then
	echo "Usage: $0 <input_image_path> <output_image_path>"
	exit 1
fi

INPUT_IMAGE="$1"
OUTPUT_IMAGE="$2"

# TODO: get input image dimensions?!

python2 "$HDRCNN_PREDICT" --params "$HDRCNN_MODEL" --im_dir "$INPUT_IMAGE" --out_dir . --width 1024 --height 512
mv 000001_out.exr "$OUTPUT_IMAGE"
rm 000001_in.png 000001_out.png
