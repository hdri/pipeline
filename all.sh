#!/bin/bash

DIR="$(dirname "$(realpath "$0")")"

. "$DIR/_settings.sh"

if [ $# -lt 1 ]; then
	echo "Usage: $0 <input_image_path>"
	exit 1
fi

HDR_IMAGE_IN="$1"

if [ ! -f "$HDR_IMAGE_IN" ]; then
	echo "Error: Input file does not exist"
	exit 1
fi

# CONVERT
HDR_IMAGE_IN_FILENAME="$(basename "$HDR_IMAGE_IN")"
HDR_IMAGE_IN_EXTENSION="$(basename "${HDR_IMAGE_IN_FILENAME##*.}")"
HDR_IMAGE_IN_NAME="$(basename "$HDR_IMAGE_IN_FILENAME" ".$HDR_IMAGE_IN_EXTENSION")"

# Convert to .exr in current directory (and limit the image size)
HDR_IMAGE="$HDR_IMAGE_IN_NAME.exr"
"$DIR"/convert.sh "$HDR_IMAGE_IN" "$HDR_IMAGE"


LDR_IMAGE="$(basename "$HDR_IMAGE").jpg"


# DEGRADE
"$DIR"/hdr_to_ldr.sh "$HDR_IMAGE" "$LDR_IMAGE"

"$DIR"/evaluate.sh evaluate.csv "$HDR_IMAGE LDR" "$HDR_IMAGE" "$LDR_IMAGE"

# RECONSTRUCT
HDRIs="${HDR_IMAGE}
" # List of images to send to the next stage (newline-separated: ground truth and reconstructoins)
#for NEURAL_NET in hdrcnn learninghdr drtmo expandnet; do
for NEURAL_NET in hdrcnn drtmo expandnet; do
	RECONSTRUCTED_HDR_IMAGE="${LDR_IMAGE}_${NEURAL_NET}.exr"
	echo Reconstructing with $NEURAL_NET
	"$DIR"/ldr_to_hdr_${NEURAL_NET}.sh "$LDR_IMAGE" "$RECONSTRUCTED_HDR_IMAGE"
	HDRIs="${HDRIs}$RECONSTRUCTED_HDR_IMAGE
" # append
	"$DIR"/evaluate.sh evaluate.csv "$HDR_IMAGE reconstruction $NEURAL_NET" "$HDR_IMAGE" "$RECONSTRUCTED_HDR_IMAGE"
done


# RENDER
OUTPUT_HDR_IMAGES=''
OLDIFS="$IFS"
IFS='
'
for IMAGE in $HDRIs; do
	#if [ _$x == '_' ]; then
	#	continue
	#fi
	
	OUTPUT_HDR_IMAGES="${OUTPUT_HDR_IMAGES}${IMAGE}
" # append the original image/envmap (ground truth or reconstruction)
	
	for ANGLE in $(seq "$RENDER_ROTATE_START" "$RENDER_ROTATE_STEP" "$RENDER_ROTATE_END"); do
		RENDER_OUTPUT_NAME="$IMAGE"_render_"$ANGLE".exr
		"$DIR"/render.sh "$IMAGE" "$ANGLE" "$RENDER_QUALITY_SPP" "$(basename "$RENDER_OUTPUT_NAME")"
		OUTPUT_HDR_IMAGES="${OUTPUT_HDR_IMAGES}${RENDER_OUTPUT_NAME}
" # append the render
	done
done
IFS="$OLDIFS"


# PREPARE FOR VIEWING
VIEW_OUTPUT_FOLDER="${HDR_IMAGE_IN_NAME}_view"
echo "$OUTPUT_HDR_IMAGES" | tr '\n' '\0' | xargs -0 "$DIR"/view.sh "$VIEW_OUTPUT_FOLDER"

# TODO: evaluate_view not after every input picture, but just once after processing all of them?
"$DIR"/evaluate_view.sh evaluate.csv . index.html

echo Done
