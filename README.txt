# High dynamic range reconstruction evaluation framework
A set of scripts for automating the process of measuring reconstruction quality of HDR reconstruction methods from single LDR image. It processes one or more input HDR images, degrades the dynamic range, runs each dynamic range reconstruction algorithm, computes error metrics by comparing the reconstructed images to the ground truth image and renders a scene using the reconstructed images as environment map/image-based lighting.

To run the pipeline, used tools, as well as dependencies of the compared reconstruction algorithm implementations (including the pre-trained parameters), need to be installed. Optionally some steps (e.g. rendering) or reconstruction methods may be disabled by editing the all.sh script. Paths to the other tools are specified in the _settings.sh script.

## Used tools:
 - Docker with "pfstools_tmp" container needs to be installed
 - pbrt renderer
 - "color mapping" scripts