#!/bin/bash

DIR="$(dirname "$(realpath "$0")")"

. "$DIR/_settings.sh"

if [ $# -lt 2 ]; then
	echo "Usage: $0 <output_folder> <first_input_hdr_image_path> [second_input_hdr_image_path] [...]"
	exit 1
fi

OUTPUT_FOLDER="$1"
shift

FIRST_IMAGE_NAME="$1"

mkdir -p "$OUTPUT_FOLDER"
mkdir -p "$OUTPUT_FOLDER/img"

escape_args() {
	for ARG in "$@"; do
		echo -n "\"$ARG\" "
	done
}

# TODO: parametric resolution
# TODO: this (escape_args) may be insecure - fix it - as in "HDR photo/merge/pfstools/merge.sh"
docker run --rm -v "$(pwd)":/workdir "$PFSTOOLS_DOCKER_IMAGE" bash -c "set -o pipefail; pfsin $(escape_args "$@") | pfssize --maxx 1280 --maxy 800 | (cd \"$OUTPUT_FOLDER\"; pfsouthdrhtml index.html --image-dir img --quality 4)"

IMAGE_BASE_NAME="$(basename "${FIRST_IMAGE_NAME%.*}")"
# TODO: replace '-' (and some other cahracters?) with '_'
VIEWER_PARAMETERS="$(cat "$OUTPUT_FOLDER/index.html" | tr '\n' '\f' | sed 's,.*<!-- HDRHTML BEG: Put this part at the beginning of the "body" section -->,,;s,<!-- HDRHTML END -->.*,,' | tr '\f' '\n')"

# https://bitbucket.org/hdri/hdr_viewer 52aaaa5a524308e08f77a0cf962d3e89eb87c0b5
VIEWER_CODE=$(cat <<END
<html>
    
    <head>
        
        <title>index.html</title>

<!-- HDRHTML BEG: Put this part in the "head" section -->
        <script type="text/javascript">            
/*
-----------------------------------------------
HDR HTML Viewer ver. 1.0
Author: Rafal Mantiuk
URL:    http://www.cs.ubc.ca/~mantiuk/hdrhtml.html
----------------------------------------------- */

            function set_opacity( obj, opacity ) {
              // for IE8
              obj.style.filter="progid:DXImageTransform.Microsoft.Alpha(Opacity=" + opacity*100 + ")";
              // for IE5-7
              obj.style.filter = "alpha(opacity=" + opacity*100 + ")";
              // for all other browsers
              obj.style.opacity = opacity;
            }
            
            function update_exp_text( hdr_image, new_exp ) {
               document.getElementById(hdr_image.base_name + "_exp_text").innerHTML = "EV: " + Math.round( (hdr_image.best_exp-new_exp)*10 )/10 + " f";
            }            
            
            function change_exp( hdr_image, exp_change ) {
            
            hdr_active_image = hdr_image;
            hdr_image.exposure = hdr_image.exposure + exp_change;
            
            if( hdr_image.exposure > hdr_image.f8_stops*8 ) {
              hdr_image.exposure = hdr_image.f8_stops*8;  
            } 
            else if( hdr_image.exposure < 0 ) {
              hdr_image.exposure = 0;
            }
            
            exp_cur = Math.floor(hdr_image.exposure / 8);
            exp_shar = exp_cur + 1;
             
            exp_blend = Math.round((hdr_image.exposure - exp_cur*8)*hdr_image.f_step_res);
            
            var i;
            for( i = 0; i < hdr_image.basis; i++ ) {
            var img_obj = document.getElementById(hdr_image.base_name+"_"+i);
            img_obj.src = hdr_image.image_dir + hdr_image.base_name + "_"+exp_cur+"_" + (i+1) + ".jpg";
            set_opacity( img_obj, cf[exp_blend][i] );
            }
            
            for( i = 0; i < hdr_image.shared_basis; i++ ) {
            var img_obj = document.getElementById(hdr_image.base_name+"_"+(i+hdr_image.basis));  
            img_obj.src = hdr_image.image_dir + hdr_image.base_name + "_"+exp_shar+"_" + (i+1) + ".jpg";  
            set_opacity( img_obj, cf[exp_blend][i+hdr_image.basis] );
            }
            
            var hist_left = (hdr_image.exposure-hdr_image.hist_start)*hdr_image.pix_per_fstop;
            
            drw_obj = document.getElementById(hdr_image.base_name+"_dr_wind");
            drw_obj.style.left = Math.round(hist_left) + "px";
            drw_obj.style.width = hdr_image.pix_per_fstop*8 + "px";
            set_opacity( drw_obj, 0.7 );
            
            update_exp_text( hdr_image, hdr_image.exposure );
            }    
            
            function insert_hdr_image( hdr_image, where ) {
            
            for( i = 0; i < hdr_image.basis + hdr_image.shared_basis; i++ ) {
				where.innerHTML += "<img id=\"" + hdr_image.base_name + "_" + i +
                                 "\" style=\"margin: 0px; padding: 0px; opacity: 1; width: " +
                                 hdr_image.width + "px; height: " + hdr_image.height + 
                                 "px; position: absolute; top: 0px; left: 0px\"" + 
                                 " />\n";
                                 }
            }
                      
            function getAbsX (obj) {
              var abs_x=0;
              while(obj) {
                abs_x+= obj.offsetLeft;
                obj= obj.offsetParent;
              }
              return abs_x;
            }
                      
            function dr_wind_clicked( event, hdr_image ) {
 
                var drw_obj = document.getElementById(hdr_image.base_name+"_dr_wind");
                var drw_left = drw_obj.offsetLeft;
                var drw_right = drw_obj.offsetLeft + drw_obj.offsetWidth;
 
                var hist_obj = document.getElementById(hdr_image.base_name + "_hist");
                var rel_x = event.clientX - getAbsX(hist_obj) - 5;
 
                var small_step_marg = 20;
 
                if( rel_x < drw_left ) {
                  change_exp( hdr_image, (drw_left-rel_x) < small_step_marg ? -1/3 : -1 );
                } else if( rel_x > drw_right ) {
                  change_exp( hdr_image, (rel_x-drw_right) < small_step_marg ? 1/3 : 1 );
                }
 
            }
 
            function dr_wind_mousedown( event, hdr_image ) {
 
                if(event.preventDefault) {
                  event.preventDefault();
                }
            
                var ctrl_obj = document.getElementById(hdr_image.base_name + "_dr_ctrl");
                var drw_obj = document.getElementById(hdr_image.base_name+"_dr_wind");
                set_opacity( drw_obj, 0.5 );
 
                hdr_mousedrag.hdr_image = hdr_image;
                hdr_mousedrag.start_mouse_x = event.clientX;
                hdr_mousedrag.start_win_x = drw_obj.offsetLeft + 2;
                ctrl_obj.onmousemove = dr_wind_move;
                ctrl_obj.onmouseup = dr_wind_mouseup;
            }
 
            function dr_wind_mouseup( event ) {
              event = event || window.event;
 
              hdr_image = hdr_mousedrag.hdr_image;
 
              var ctrl_obj = document.getElementById(hdr_image.base_name + "_dr_ctrl");
              var drw_obj = document.getElementById(hdr_image.base_name+"_dr_wind");
              var drw_left = drw_obj.offsetLeft;
 
              var new_exp = Math.round((drw_obj.offsetLeft/hdr_image.pix_per_fstop + hdr_image.hist_start)*3)/3;
 
              ctrl_obj.onmousemove = null;
              ctrl_obj.onmouseup = null;
              hdr_mousedrag.hdr_image = null;
 
              change_exp( hdr_image, new_exp - hdr_image.exposure );
            }
 
 
            function dr_wind_move(event){ 
              event = event || window.event;
 
              hdr_image = hdr_mousedrag.hdr_image;
              var drw_obj = document.getElementById(hdr_image.base_name+"_dr_wind");  
 
              var new_pos = (event.clientX-hdr_mousedrag.start_mouse_x) + hdr_mousedrag.start_win_x;
              drw_obj.style.left = new_pos + "px";
 
              var new_exp = Math.round((drw_obj.offsetLeft/hdr_image.pix_per_fstop + hdr_image.hist_start)*3)/3;
              update_exp_text( hdr_image, new_exp );
            }

            function hdr_show_help() {
                newwindow=window.open('','name','height=400,width=300');
                var tmp = newwindow.document;
                tmp.write('<html><head><title>HDR HTML viewer help</title>');
                tmp.write('</head><body>');
                tmp.write('<a href="http://pfstools.sourceforge.net/hdrhtml/" target="_blank">HDR HTML viewer</a> <div style="font-size: small">ver. 1.0</div>');
                tmp.write('<div style="font-size: small">(C) 2008 Rafal Mantiuk</div>');
                tmp.write('<ul style="font-size: small">');
                tmp.write('<li>Change exposure by scrolling the blue dynamic range window left and right.</li>');
                tmp.write('<li>Click on the left or right side of the dynamic range window to change exposure by either 1 or 1/3 of an f-stop depending on how far from the window you click.</li>');
                tmp.write('<li>Press "-" and "=" keys to change exposure by 1 f-stop.</li>');
                tmp.write('<li>The green plot represents image histogram. Each tick represent one f-stop. EV value is given in f-stops (log<sub>2</sub> units) relative to the inital exposure.</li>');
                tmp.write('</ul>');
                tmp.write('</body></html>');
                tmp.close();
            }

            function hdr_onkeydown(e) {              
              var hdr_image = hdr_active_image;
              var keynum;
              if( hdr_image == null )
                  return;
                
              if(window.event) { // IE
                keynum = window.event.keyCode;
                if( keynum == 189 )
                  change_exp( hdr_image, +1 );
                if( keynum == 187 )
                  change_exp( hdr_image, -1 );                                
              } else if(e.which) { // Netscape/Firefox/Opera                
                keynum = e.which;
                if( keynum == 109 )
                  change_exp( hdr_image, +1 );
                if( keynum == 61 )
                  change_exp( hdr_image, -1 );                
              }
                
           }
            
            
        </script>
<!-- HDRHTML END -->
        
    </head>
    
    <body style="background-color: gray">

<!-- HDRHTML BEG: Put this part at the beginning of the "body" section -->                
$VIEWER_PARAMETERS
<!-- HDRHTML END -->                
        
		<script>
			var viewerSettings = null;
			
			function UpdateViewer(form) {
				
				var last_exp_offset = 0;
				
				if (viewerSettings) {
					var last_exp_offset = viewerSettings.exposure - viewerSettings.best_exp;
				}
				
				var groundtruth = form.groundtruth.checked;
				var reconstruction = form.reconstruction.value;
				var image_type = form.image_type.value;
				
				form.reconstruction.disabled = groundtruth;
				
				//var base_name = 'Etnies_Park_Center_3k' // TODO: base name
				var base_name = '$IMAGE_BASE_NAME'; // TODO: base name
				
				var name = base_name;
								
				if (!groundtruth) {
					name += '_exr_jpg_' + reconstruction;					
					
				}
				
				if (image_type != '') {
					name += '_exr_' + image_type;
					
				}
				
				console.log(name);
				
				name = name.replace('-', '_');
				
				viewerSettings = window['hdr_' + name];
				
				document.getElementById('viewer_main').innerHTML = '\
        <div id="' + name + '_dr_ctrl" style="position: relative; width: ' + viewerSettings.width + 'px; height: 36px; overflow: hidden; border: 2px solid black;" title="Drag dynamic range window to change exposure">\
                <img id="' + name + '_hist" style="position: absolute; left: 0px; top: 0px;" src="img/' + name + '_hist.png" onclick="dr_wind_clicked(event, hdr_' + name + ')"/>\
                <div id="' + name + '_dr_wind" style="position: absolute; left: 100px; top: 0px; width: 40px; height: 36px; opacity: 0.7; background-color: blue;" onmousedown="dr_wind_mousedown(event, hdr_' + name + ')"></div>\
                <div id="' + name + '_exp_text" style="color: white; font-family: cursive; position: absolute; left: 10px; top: 2px;">1</div>\
                <a style="color: yellow; position: absolute; left: 1268px; top: 6px;" href="#" title="Click to get help" onclick="hdr_show_help()">?</a>\
        </div>\
        <div id="active_viewer" style="position: relative; border: 2px solid black; background-color: black; color: white; width: ' + viewerSettings.width + 'px; height: ' + viewerSettings.height + 'px">\
            \
        </div>\
				';
				
				viewerSettings.exposure = viewerSettings.best_exp + last_exp_offset;
				
                insert_hdr_image( viewerSettings, document.getElementById('active_viewer') );
				
				change_exp(viewerSettings, 0);
			}
		</script>
		
<form id="viewerSettingsForm" onchange="UpdateViewer(this)">
	<input id="groundtruth" name="groundtruth" type="checkbox" /> <label for="groundtruth">Ground truth</label>
	
	<select name="reconstruction">
	  <option value="hdrcnn">hdrcnn</option>
	  <!--<option value="learninghdr">learninghdr</option>-->
	  <option value="drtmo">drtmo</option>
	  <option value="expandnet">expandnet</option>
	</select>
	
	<select name="image_type">
	  <option value="">HDRi</option>
	  <option value="render_0">Render 0</option>
	  <option value="render_180">Render 180</option>
	  <!--<option value="render_60">Render 60</option>
	  <option value="render_120">Render 120</option>
	  <option value="render_240">Render 240</option>
	  <option value="render_300">Render 300</option>-->
	</select>
</form>

	<div id="viewer_main">
		(to be replaced by JavaScript)
    </div>
        
		<script>
			UpdateViewer(document.getElementById('viewerSettingsForm'));
		</script>
		
    </body>
</html>

END
)
	
echo "$VIEWER_CODE" > "$OUTPUT_FOLDER/viewer.html"
