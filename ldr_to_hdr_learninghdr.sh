#!/bin/bash

#DIR=$(dirname "$0")
DIR="$(dirname "$(realpath "$0")")"

. "$DIR/_settings.sh"

if [ $# -lt 2 ]; then
	echo "Usage: $0 <input_image_path> <output_image_path>"
	exit 1
fi

INPUT_IMAGE="$1"
OUTPUT_IMAGE="$2"

# TODO: "rotate"/"cyclic scroll" the image horizontally so that the sun is in the middle
python2 "$LEARNINGHDR_PREDICT" -m "$LEARNINGHDR_MODEL" -i "$INPUT_IMAGE"
# TODO: revert the "rotation"

mv pred.exr "$OUTPUT_IMAGE"
