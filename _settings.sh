#!/bin/sh

# stop on error
set -e

# stop on using an unset variable
set -u

# stop if any command in pipeline fails
set -o pipefail

# print executed commands (debug)
set -x

#set -v

# TODO: run only once? ("ifndef x, define x", but in shell? would "exit 0" work?)

if [ -z ${DIR+x} ]; then
	echo "DIR needs to be set to this script's path"
	exit 1
fi

CONVERT_MAXX=1024
CONVERT_MAXY=512

HDRCNN_PATH="$DIR/../HDRCNN/cnn/hdrcnn"
HDRCNN_PREDICT="$HDRCNN_PATH/hdrcnn_predict.py"
HDRCNN_MODEL="$HDRCNN_PATH/hdrcnn_params.npz"

LEARNINGHDR_PATH="$DIR/../learningHDR/ldr2hdr-public"
LEARNINGHDR_PREDICT="$LEARNINGHDR_PATH/ldr2hdr.py"
LEARNINGHDR_MODEL="$LEARNINGHDR_PATH/model_DomainAdapt"

DRTMO_PATH="$DIR/../DrTMO/DrTMO"
DRTMO_PREDICT="$DRTMO_PATH/main.py"
DRTMO_MODELDOWN="$DRTMO_PATH/models/downexposure_model.chainer"
DRTMO_MODELUP="$DRTMO_PATH/models/upexposure_model.chainer"

EXPANDNET_PATH="$DIR/../ExpandNet/hdr-expandnet"
EXPANDNET_PREDICT="$EXPANDNET_PATH/expand.py"
EXPANDNET_MODEL="$EXPANDNET_PATH/weights.pth"


PBRT_PATH="$DIR/../pbrt-v3"
PBRT_BIN="$PBRT_PATH/build/pbrt"

RENDER_WIDTH=1024
RENDER_HEIGHT=333

RENDER_ROTATE_START=0
RENDER_ROTATE_STEP=180
RENDER_ROTATE_END=359

RENDER_QUALITY_SPP=8


PFSTOOLS_DOCKER_IMAGE="pfstools_tmp"

COMPARE_IMAGES_PATH="$DIR/../color_mapping"
COMPARE_IMAGES_GET_DIFFERENCE="$COMPARE_IMAGES_PATH/get_difference.py"
COMPARE_IMAGES_GET_MAPPING_AND_PLOT="$COMPARE_IMAGES_PATH/get_mapping_and_plot.sh"
COMPARE_IMAGES_FIX_MAPPING="$COMPARE_IMAGES_PATH/fix_mapping_auto.py"
