#!/bin/bash

DIR="$(dirname "$(realpath "$0")")"

. "$DIR/_settings.sh"

if [ $# -lt 2 ]; then
	echo "Usage: $0 <input_hdr_image> <output_hdr_image>"
	exit 1
fi

INPUT_IMAGE="$1"
OUTPUT_IMAGE="$2"

# -v "...:/input/...": Make the input file accessible from instde the container (in /input folder)
# TODO: this seems to COPY the file into the container. Is there a more efficient way to do this? (piping the file to docker is too slow; symlink outside of the container gets resolved INSIDE the container, so it doesn't work; maybe mounting the FOLDER where the input file is located would be better?)
docker run --rm -v "$(pwd)":/workdir -v "$INPUT_IMAGE":"/input/$(basename "$INPUT_IMAGE")" "$PFSTOOLS_DOCKER_IMAGE" bash -c "set -o pipefail; pfsin --linear \"/input/$(basename "$INPUT_IMAGE")\" | pfssize --maxx \"$CONVERT_MAXX\" --maxy \"$CONVERT_MAXY\" | pfsout \"$OUTPUT_IMAGE\""
# If pfsin fails with "Corrupted PFS file: missing channel data" on a (large) file you are certain is valid, maybe a "short fread" has occurred due to lack of RAM - try looking at RAM/SWAP usage inside the container (e.g. using htop) while the command runs, possibly increase Docker's memory limit