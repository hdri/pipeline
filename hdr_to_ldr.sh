#!/bin/bash

#DIR=$(dirname "$0")
DIR="$(dirname "$(realpath "$0")")"

. "$DIR/_settings.sh"

if [ $# -lt 2 ]; then
	echo "Usage: $0 <input_image_path> <output_image_path>"
	exit 1
fi

INPUT_IMAGE="$1"
OUTPUT_IMAGE="$2"

docker run --rm -v "$(pwd)":/workdir "$PFSTOOLS_DOCKER_IMAGE" bash -c "set -o pipefail; pfsin \"$INPUT_IMAGE\" | pfsoutimgmagick --srgb --quality 100 \"$OUTPUT_IMAGE\""
