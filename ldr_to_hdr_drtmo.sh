#!/bin/bash

#DIR=$(dirname "$0")
DIR="$(dirname "$(realpath "$0")")"

. "$DIR/_settings.sh"

if [ $# -lt 2 ]; then
	echo "Usage: $0 <input_image_path> <output_image_path>"
	exit 1
fi

INPUT_IMAGE="$1"
OUTPUT_IMAGE="$2"

TMPOUTDIR="$(basename "$INPUT_IMAGE").DrTMOtmp"

mkdir -p "$TMPOUTDIR"

python2 "$DRTMO_PREDICT" -dm "$DRTMO_MODELDOWN" -um "$DRTMO_MODELUP" -i "$INPUT_IMAGE" -o "$TMPOUTDIR"

# Convert hdr to exr
docker run --rm -v "$(pwd)":/workdir "$PFSTOOLS_DOCKER_IMAGE" bash -c "set -o pipefail; pfsin \"$TMPOUTDIR/$(basename "$INPUT_IMAGE")/MergeDebevec.hdr\" | pfsout \"Merge.exr\""

# TODO: try if MergeRobertson gives better results

mv "Merge.exr" "${OUTPUT_IMAGE}_nonlinearized.exr"

# Linearize the reconstructed image (so that the 0..1 pixel value range is changed as little as possible)
python3 "$COMPARE_IMAGES_FIX_MAPPING" "$INPUT_IMAGE" "${OUTPUT_IMAGE}_nonlinearized.exr" "${OUTPUT_IMAGE}"

# Delete the remaining files
rm -r "$TMPOUTDIR"
