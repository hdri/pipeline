#!/bin/bash

DIR="$(dirname "$(realpath "$0")")"

#. "$DIR/_settings.sh"

if [ $# -lt 1 ]; then
	echo "Usage: $0 <input_folder_or_file_path>"
	exit 1
fi

INPUT_PATH="$1"
# TODO: realpath

if [ ! -e "$INPUT_PATH" ]; then
	echo "Error: Input path does not exist"
	exit 1
fi

(
# Set field separator to "\n" (ignore spaces in file paths, each found file is on one line)
IFS='
'
for IMAGE in $(find "$INPUT_PATH" -type f -iname '*.exr' -o -iname '*.hdr'); do
	echo Processing "$IMAGE"
	# TODO: do not stop on error
	# TODO: run in parallel for increased speed?
	# TODO: skip already processed files?
	# TODO: better handling of Ctrl+C (do not continue)
	(
	BASENAME="$(basename "$IMAGE")"
	LOGFILE="$BASENAME.log"
	# Run the pipeline for this image file, save output (stdout+stderr) to a log file
	time sh -c "\"$DIR/all.sh\" \"$(realpath "$IMAGE")\" >\"$LOGFILE\" 2>&1"
	RESULT="$?"
	echo
	echo Return code: $RESULT
	echo
	)
done
)