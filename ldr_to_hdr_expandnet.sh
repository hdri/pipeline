#!/bin/bash

#DIR=$(dirname "$0")
DIR="$(dirname "$(realpath "$0")")"

. "$DIR/_settings.sh"

if [ $# -lt 2 ]; then
	echo "Usage: $0 <input_image_path> <output_image_path>"
	exit 1
fi

INPUT_IMAGE="$1"
OUTPUT_IMAGE="$2"


python3 "$EXPANDNET_PREDICT" --model "$EXPANDNET_MODEL" --out . "$INPUT_IMAGE"

PREDICTION_FILENAMEBASE="$(basename "$INPUT_IMAGE" .jpg)_prediction"

# Convert hdr to exr
docker run --rm -v "$(pwd)":/workdir "$PFSTOOLS_DOCKER_IMAGE" bash -c "set -o pipefail; pfsin \"$PREDICTION_FILENAMEBASE.hdr\" | pfsout \"$PREDICTION_FILENAMEBASE.exr\""

rm "$PREDICTION_FILENAMEBASE.hdr"

mv "$PREDICTION_FILENAMEBASE.exr" "${OUTPUT_IMAGE}_nonlinearized.exr"

# Linearize the reconstructed image (so that the 0..1 pixel value range is changed as little as possible)
python3 "$COMPARE_IMAGES_FIX_MAPPING" "$INPUT_IMAGE" "${OUTPUT_IMAGE}_nonlinearized.exr" "${OUTPUT_IMAGE}"
