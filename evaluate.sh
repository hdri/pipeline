#!/bin/bash

DIR="$(dirname "$(realpath "$0")")"

. "$DIR/_settings.sh"

if [ $# -lt 3 ]; then
	echo "Usage: $0 <output_csv> <label> <ground_truth_hdr_image> <reconstruction_hdr_image>"
	exit 1
fi

OUTPUT_CSV="$1"
LABEL="$2"
GROUND_TRUTH_IMAGE_NAME="$3"
RECONSTRUCTION_IMAGE_NAME="$4"

# generate evaluation (error metric) csv

if [ ! -e "$OUTPUT_CSV" ]; then
	echo 'label,ground_truth_image,reconstruction_image,saturation,all_l2,saturated_l2,unsaturated_l2,ssim' > "$OUTPUT_CSV"
fi

LINE="${LABEL},$(basename "$GROUND_TRUTH_IMAGE_NAME"),$(basename "$RECONSTRUCTION_IMAGE_NAME")"

RESULT="$(python2 "$COMPARE_IMAGES_GET_DIFFERENCE" "$GROUND_TRUTH_IMAGE_NAME" "$RECONSTRUCTION_IMAGE_NAME")"

ALL_L2="$(echo "$RESULT" | grep '^avg sqrt(sqr_difference_total) per pixel' | sed 's/.*: //')"
SATURATED_L2="$(echo "$RESULT" | grep '^saturated_pixels avg sqrt(sqr_difference_total) per pixel' | sed 's/.*: //')"
UNSATURATED_L2="$(echo "$RESULT" | grep '^unsaturated_pixels avg sqrt(sqr_difference_total) per pixel' | sed 's/.*: //')"
SATURATION="$(echo "$RESULT" | grep '^saturation' | sed 's/.*: //')"
SSIM="$(echo "$RESULT" | grep '^ssim' | sed 's/.*: //')"

LINE="${LINE},$SATURATION,$ALL_L2,$SATURATED_L2,$UNSATURATED_L2,$SSIM"

echo "$LINE" >> "$OUTPUT_CSV"


# get_mapping_and_plot.sh

"$COMPARE_IMAGES_GET_MAPPING_AND_PLOT" "$GROUND_TRUTH_IMAGE_NAME" "$RECONSTRUCTION_IMAGE_NAME"
